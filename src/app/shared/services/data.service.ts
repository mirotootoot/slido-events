import { UrlParam } from '../models/UrlParam';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export abstract class DataService {

  constructor(protected httpClient: HttpClient) { }

  protected abstract dataUrl(): string;

  protected GetBaseUrl(): string {
    return environment.baseUrl + this.dataUrl();
  }

  protected httpGet<T>(url: string, urlParams: UrlParam[], apiVersion: string = ""): Observable<T> {
    var httpOptions = {
      headers: this.getDefaultHeaders(apiVersion),
      params: this.getUrlParams(urlParams)
    }
    return this.httpClient.get<T>(this.GetBaseUrl() + url, httpOptions).pipe(
      catchError(
        this.handleError('httpGet', null)
      )
    );
  }

  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }

  protected getDefaultHeaders(apiVersion: string = ""): HttpHeaders {
    var result = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': this.getAcceptHeader(apiVersion),
    });
    return result;
  }

  protected getUrlParam(name: string, value: Object): UrlParam {
    return new UrlParam(name, value == null ? "" : String(value));
  }

  private getUrlParams(urlParams: UrlParam[]): HttpParams {
    var result = new HttpParams();
    if (!urlParams) {
      return result;
    }
    urlParams.forEach(element => {
      result = result.set(element.key, element.value)
    });
    return result;
  }

  private getAcceptHeader(version: string) {
    var result = 'application/vnd.tootoot.v';
    if (!version || version == "") {
      result += '2.0';
    } else {
      result += version;
    }
    result += '+json';
    return result;
  }
}

