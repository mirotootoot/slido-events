import { EntityRef } from './EntityRef';

export class ProfileRef extends EntityRef {
    ProfileName: string;
    ProfilePicture: string;
    constructor(profileRef: ProfileRef) {
        super();
        Object.assign(this, profileRef);
    }
}