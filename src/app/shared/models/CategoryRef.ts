import { EntityRef } from './EntityRef';

export class CategoryRef extends EntityRef {
    public Name: string;
    constructor(categoryRef: CategoryRef) {
        super();
        Object.assign(this, categoryRef);
    }
}