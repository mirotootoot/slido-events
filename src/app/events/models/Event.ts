import { AddressContact } from './AddressContact';
import { LonLat } from './LonLat';
import { EventBuildingRef } from './EventBuildingRef';
import { CategoryRef } from 'src/app/shared/models/CategoryRef';
import { ProfileRef } from 'src/app/shared/models/ProfileRef';
import { environment } from 'src/environments/environment';


export class Event {

    _id: string;
    ProfileName: string;
    ProfilePicture: string;
    About: string;
    AboutRichText: string;
    Begin: Date;
    End: Date;
    Author: ProfileRef;
    Building: EventBuildingRef;
    AddressContact: AddressContact;
    LonLat: LonLat
    Categories: CategoryRef[];

    constructor()
    constructor(event: Event)
    constructor(event?: Event) {
        if (event == null) {
            this.setDefaultValues();
            return;
        }
        Object.assign(this, event);
        this.Building = new EventBuildingRef(event?.Building);
        this.AddressContact = new AddressContact(event?.AddressContact);
        this.LonLat = new LonLat(event?.LonLat);
        this.Categories = this.getCategories(event.Categories);
        this.Author = new ProfileRef(event.Author);
        this.Begin = new Date(event.Begin);
        this.End = new Date(event.End);
    }

    private setDefaultValues() {
        this._id = new Date().getTime().toString();
        this.ProfileName = "";
        this.ProfilePicture = "";
        this.About = "";
        this.AboutRichText = "";
        this.Begin = new Date();
        this.End = new Date();
        this.Author = new ProfileRef({
            EntityType: "Profile",
            ProfileName: "",
            ProfilePicture: "",
            _id: ""
        })
        this.Building = new EventBuildingRef({
            EntityType: "Profile",
            ProfileName: "",
            ProfilePicture: "",
            _id: "",
            Confirmed: true
        })
        this.AddressContact = new AddressContact({
            AddressLine: "",
            City: "",
            Country: "",
            Zip: ""
        })
        this.LonLat = new LonLat({ Lat: 0, Lon: 0 });
        this.Categories = [];
    }

    private getCategories(categories: CategoryRef[]): CategoryRef[] {
        var result = [];
        if (categories == null) {
            return result;
        }
        categories.forEach(category => {
            result.push(new CategoryRef(category));
        });
        return result;
    }

    getImageUrl(width: number, height: number) {
        if (this.ProfilePicture.startsWith("http")) {
            return this.ProfilePicture;
        }
        return environment.baseUrl + "event/" + this._id + "/images/" + this.ProfilePicture + "/" + width + "/" + height + "/AUTO";
    }

    isValidToCreate(): boolean {
        return this.isValidBeginEnd() && 
        this.ProfileName != null && 
        this.ProfileName.length > 3 && 
        this.isValidLocation() &&
        this.Author.ProfileName != null && 
        this.Author.ProfileName.length > 3 ;
    }

    private isValidBeginEnd(): boolean {
        return this.Begin != null && this.End != null && this.Begin < this.End;
    }

    private isValidLocation(): boolean {
        return this.Building.ProfileName != null &&
            this.AddressContact.AddressLine != null &&
            this.AddressContact.City != null &&
            this.AddressContact.Country != null;
    }

}