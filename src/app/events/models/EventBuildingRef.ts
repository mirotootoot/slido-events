import { ProfileRef } from '../../shared/models/ProfileRef';

export class EventBuildingRef extends ProfileRef {
    Confirmed: boolean;

    constructor(eventBuildingRef: EventBuildingRef) {
        super(eventBuildingRef);
        Object.assign(this, eventBuildingRef);
    }
}