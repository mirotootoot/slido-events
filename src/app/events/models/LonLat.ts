export class LonLat {
    Lon: number;
    Lat: number;
    constructor(lonLat: LonLat) {
        Object.assign(this, lonLat);
    }
}