export class AddressContact {
    AddressLine: string = "";
    City: string = "";
    Zip: string = "";
    Country: string = "";

    constructor(addressContact: AddressContact) {
        Object.assign(this, addressContact);
    }
}