import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../../models/Event'
import { EventDataService } from '../../services/event-data.service';
import { MatDialog } from '@angular/material/dialog';
import { EventDetailComponent } from '../event-detail/event-detail.component';
import { EventCreateComponent } from '../event-create/event-create.component';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  @Input()
  Events: Event[];
  FutureEvents: Event[] = [];
  PastEvents: Event[] = [];
  QuickSearchKey: string;
  FilteredEvents: Event[] = [];

  constructor(private eventDataService: EventDataService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllEvents();
  }

  onEventSelect(eventId: string) {
    console.log(eventId);
    if (!eventId)
      return;
    var event = this.Events.find(e => e._id == eventId);
    this.dialog.open(EventDetailComponent, {
      data: event,
      maxWidth: "90%",
    });
  }

  onEventCreate() {
    let dialogRef = this.dialog.open(EventCreateComponent, {
    });
    dialogRef.afterClosed().subscribe(event => {
      if (!event)
        return;
      event.Begin = new Date(event.Begin);
      event.End = new Date(event.End);
      this.addEvent(event);
    });
  }

  getAllEvents(): void {
    this.Events = [];
    this.eventDataService.getAll()
      .subscribe(
        (events: Event[]) => {
          if (!events) {
            return;
          }
          this.resetEvents(events);
        },
        error => {
          console.log('events loading failed: ' + error)
        }
      );
  }

  quickSearch($key): void {
    if (!$key) {
      this.QuickSearchKey = '';
      this.FilteredEvents = [];
      return;
    }
    this.FilteredEvents = this.getFilteredEvents(this.normalize($key));
  };

  private resetEvents(events: Event[]) {
    this.Events = events;
    var now = new Date();
    this.FutureEvents = this.Events.filter(e => e.End > now).sort((a, b) => (a.End > b.End ? 1 : -1));
    this.PastEvents = this.Events.filter(e => e.End < now).sort((a, b) => (a.End < b.End ? 1 : -1));
  }

  private addEvent(event: Event) {
    this.Events.push(event);
    var now = new Date();
    if (event.End > now) {
      this.FutureEvents.push(event);
    } else {
      this.PastEvents.push(event);
    }
  }

  private getFilteredEvents(key: string): Event[] {
    return this.Events.filter(e =>
      this.includes(e.ProfileName, key) ||
      this.includes(e.AddressContact.City, key) ||
      this.includes(e.Author.ProfileName, key) ||
      this.includes(e.Building.ProfileName, key) ||
      this.includes(e._id, key));
  }

  private includes(input: string, key: string): boolean {
    return this.normalize(input).includes(key);
  }

  private normalize(input: string): string {
    return input.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
}
