import { Component, OnInit } from '@angular/core';
import { Event } from 'src/app/events/models/Event';
import { CategoryRef } from 'src/app/shared/models/CategoryRef';
import { ProfileRef } from 'src/app/shared/models/ProfileRef';
import { AddressContact } from '../../models/AddressContact';
import { EventBuildingRef } from '../../models/EventBuildingRef';
import { LonLat } from '../../models/LonLat';


@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.scss']
})
export class EventCreateComponent implements OnInit {

  event: Event = new Event();
  newCategory: string = "";

  constructor() { }

  ngOnInit() {

  }

  addCategory(): void {
    if (!this.newCategory || this.event.Categories.findIndex(e => e.Name == this.newCategory) != -1)
      return;
    this.event.Categories.push(new CategoryRef({ EntityType: "Category", _id: "", Name: this.newCategory }));
    this.newCategory = "";
  }

  removeCategory(category: string): void {
    var index = this.event.Categories.findIndex(e => e.Name == category);
    if (index > -1) {
      this.event.Categories.splice(index, 1);
    }
  }

  getEventModel(): Event {
    this.event.Begin = new Date(this.event.Begin);
    this.event.End = new Date(this.event.End);
    return this.event;
  }
}
