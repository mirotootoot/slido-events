import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {
  
  @Input()
  Events: Event[];
  @Input()
  Title: string;
  @Output()
  EventSelected = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
  }

  onEventSelect(eventId: string) {
    this.EventSelected.emit(eventId);
  }

}
