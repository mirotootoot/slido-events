import { Component, OnInit, Inject } from '@angular/core';
import { Event } from '../../models/Event';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  Event: Event;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Event) {
    this.Event = data;
  }

  ngOnInit(): void {
  }

}
