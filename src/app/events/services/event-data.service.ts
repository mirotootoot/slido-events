import { Injectable } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Event } from '../models/Event';
import { UrlParam } from '../../shared/models/UrlParam';
import { EventView } from 'src/app/events/models/EventView';

@Injectable({
  providedIn: 'root'
})
export class EventDataService extends DataService {

  protected dataUrl(): string {
    return "event/";
  }

  getById(eventId: string): Observable<Event> {
    return this.httpGet<Event>(eventId, null, '2.1')
      .pipe(
        map((event) => new Event(event))
      );
  }

  getByProfileId(profileId: string): Observable<Event[]> {
    const params = new Array<UrlParam>();
    params.push(new UrlParam('profileId', profileId));
    params.push(new UrlParam('page', '0'))
    params.push(new UrlParam('perPage', '100'))
    return this.httpGet<Event[]>('', params, '2.1')
      .pipe(
        map(events => events ? events.map(e => new Event(e)) : null)
      );
  }

  getAll(): Observable<Event[]> {
    var now = new Date();
    var since = new Date();
    since.setDate(now.getDate() - 1);
    var till = new Date();
    till.setDate(now.getDate() + 1);

    const params = new Array<UrlParam>();
    params.push(new UrlParam('categories', ''));
    params.push(new UrlParam('cityId', ''));
    params.push(new UrlParam('page', '0'))
    params.push(new UrlParam('perPage', '100'))
    params.push(new UrlParam('since', since.toISOString()));
    params.push(new UrlParam('till', till.toISOString()));
    return this.httpGet<EventView[]>('/search', params, '2.0')
      .pipe(
        map(events => events ? events.map(e => new Event(e.Event)) : null)
      );
  }
}
