import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events/components/events/events.component';
import { AppRoutes } from 'src/app/shared/enums/AppRoutes';

const routes: Routes = [
  { path: AppRoutes.EVENTS, component: EventsComponent },
  { path: '**', redirectTo: AppRoutes.DEFAULT_COMPONENT }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
